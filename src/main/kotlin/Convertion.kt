import kotlin.math.pow

/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
class Convertion {
    //convert of units of Metric System
    //Km to metters
    fun kmToM1(km: Double): Int {
        return (km*1000).toInt()
    }

    //Km to metters
    fun kmTom(km: Double): Double {
        return km*1000
    }

    //Km to cm
    fun kmTocm(km: Double): Double {
        return km/100000
    }

    //milimetters to metters
    fun mmTom(mm: Int): Double {
        return mm.toDouble()/1000
    }

    //convert of units of U.S Standard System
    //convert miles to foot
    fun milesToFoot(miles: Double): Double {
        return miles*5280
    }

    //convert yards to inches
    fun yardToInch(yard: Int): Int {
        return yard*36
    }

    //convert inches to miles
    fun inchToMiles(inch: Double): Double {
        return inch*(1.57828.pow(-5.0))
    }

    //convert foot to yards
    fun footToYard(foot: Int): Int {
        return (foot*0.3333).toInt()
    }

    //Convert units in both systems
    //convert Km to inches
    fun kmToInch(km: String?): Double {
        val kmtoinches = km?.toDoubleOrNull()
        return (kmtoinches!! * (1000 / 1) * (100 / 1) * (1 / 2.54))
    }

    //convert milimmeters to foots
    fun mmToFoot(mm: String?): Double {
        val mmtofoots = mm?.toDouble()
        return (mmtofoots!! * (1 / 304.8))
    }

    //convert yards to cm
    fun yardToCm(yard: String?): Double {
        val num = yard?.toDouble()
        val  A = (num!! * 91.44)
        return A
    }
}