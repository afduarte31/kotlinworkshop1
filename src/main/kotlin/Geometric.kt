/**
 * @author sigmotoa
 */
import kotlin.math.*

class Geometric {
    //Calculate the area for a square using one side
    fun squareArea(side: Int): Int {
        return side*side
    }

    //Calculate the area for a square using two sides
    fun squareArea(sideA: Int, sideB: Int): Int {
        return sideA*sideB
    }

    //Calculate the area of circle with the radius
    fun circleArea(radius: Double): Double {
        return PI*radius.pow(2.0)
    }

    //Calculate the perimeter of circle with the diameter
    fun circlePerimeter(diameter: Int): Double {
        return PI*diameter.toDouble()
    }

    //Calculate the perimeter of square with a side
    fun squarePerimeter(side: Double): Double {
        return side.pow(4.0)
    }

    //Calculate the volume of the sphere with the radius
    fun sphereVolume(radius: Double): Double {
        return ((4*PI*radius.pow(3.0))/3)
    }

    //Calculate the area of regular pentagon with one side
    fun pentagonArea(side: Int): Float {
        val result = ((sqrt(5.00 * (5.00 + 2.00 * (sqrt(5.0).toFloat()))).toFloat() * side.toFloat() * side.toFloat()) / 4.00).toFloat()
        val fin = (round(result * 10.0) / 10.0).toFloat()
        return fin
    }

    //Calculate the Hypotenuse with two legs
    fun calculateHypotenuse(legA: Double, legB: Double): Double {
        return hypot(legA, legB)
    }
}